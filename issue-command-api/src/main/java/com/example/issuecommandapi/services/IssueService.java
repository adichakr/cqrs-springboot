package com.example.issuecommandapi.services;

import com.example.issuecommandapi.dto.CreateIssueRequest;

public interface IssueService {

    String createIssue(CreateIssueRequest createIssueRequest);

}
