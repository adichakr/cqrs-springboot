package com.example.issuecommandapi.services;

import com.example.issuecommandapi.commands.CreateIssueCommand;
import com.example.issuecommandapi.dto.CreateIssueRequest;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class IssueServiceImpl implements IssueService {
    private final CommandGateway commandGateway;

    public IssueServiceImpl(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public String createIssue(CreateIssueRequest createIssueRequest) {
        log.info("CreateIssueRequest ----> {}", createIssueRequest);
        CreateIssueCommand createIssueCommand = new CreateIssueCommand();
        BeanUtils.copyProperties(createIssueRequest, createIssueCommand);
        createIssueCommand.setIssueId(UUID.randomUUID().toString());
        createIssueCommand.setIssueStatus("Open");
        log.info("createIssueCommand ----> {}", createIssueCommand);
        String issueId = commandGateway.sendAndWait(createIssueCommand);
        return issueId;
    }
}
