package com.example.issuecommandapi.commands;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateIssueCommand {
    @TargetAggregateIdentifier
    private String issueId;
    private String issueTitle;
    private String issueDescription;
    private String issueStatus;
    private String assignedGroup;
    private Integer issueSeverity;

}
