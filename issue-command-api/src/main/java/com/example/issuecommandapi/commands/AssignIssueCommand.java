package com.example.issuecommandapi.commands;

import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@Builder
public class AssignIssueCommand {
    @TargetAggregateIdentifier
    private String issueId;
    private String issueStatus;
    private String assignedGroup;
}
