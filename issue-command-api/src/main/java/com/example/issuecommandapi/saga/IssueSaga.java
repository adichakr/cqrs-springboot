package com.example.issuecommandapi.saga;

import com.example.corelib.commands.CreateJiraTicketCommand;
import com.example.corelib.events.IssueAssignedEvent;
import com.example.corelib.events.IssueCreatedEvent;
import com.example.issuecommandapi.commands.AssignIssueCommand;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.distributed.CommandDispatchException;
import org.axonframework.commandhandling.distributed.DistributedCommandBus;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@Saga
@Slf4j
public class IssueSaga {
    @Autowired
    private transient CommandGateway commandGateway;


    public IssueSaga() {

    }

    @SagaEventHandler(associationProperty = "issueId")
    @StartSaga

    public void on(IssueCreatedEvent issueCreatedEvent) {
        log.info("Saga is handling the events ==> IssueCreatedEvent");
        commandGateway.sendAndWait(AssignIssueCommand.builder().issueId(issueCreatedEvent.getIssueId())
                .assignedGroup("IT-L0").issueStatus("Assigned").build());
    }

    @SagaEventHandler(associationProperty = "issueId")
    @EndSaga
    public void on(IssueAssignedEvent issueAssignedEvent) {
        log.info("Saga is handling the events==> issueAssignedEvent {}", issueAssignedEvent.getIssueId());
        try {
            commandGateway.send(CreateJiraTicketCommand.builder().issueId(issueAssignedEvent.getIssueId()).jiraNumber("JR-01").jiraStatus("Open").ticketOwner("dummyuser").build());
        } catch (CommandDispatchException e) {
            e.printStackTrace();
        }
    }


}
