package com.example.issuecommandapi.controllers;

import com.example.issuecommandapi.dto.CreateIssueRequest;
import com.example.issuecommandapi.services.IssueService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/issues")
public class IssueController {

    private final IssueService issueService;


    public IssueController(IssueService issueService) {
        this.issueService = issueService;
    }

    @PostMapping
    public ResponseEntity<String> createIssue(@RequestBody CreateIssueRequest createIssueRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(issueService.createIssue(createIssueRequest));

    }
}
