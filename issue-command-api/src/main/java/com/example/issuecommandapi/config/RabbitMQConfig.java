package com.example.issuecommandapi.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Bean
    public Exchange exchannge() {
        Exchange issueExchange = ExchangeBuilder.fanoutExchange("IssueExchange").build();
        return issueExchange;
    }

    @Bean
    public Queue queue() {
        return QueueBuilder.durable("IssueQueue").build();
    }


    @Bean
    public Binding binding() {
        return BindingBuilder.bind(queue()).to(exchannge()).with("*").noargs();
    }

    @Autowired
    public void configure(AmqpAdmin amqpAdmin) {
        amqpAdmin.declareExchange(exchannge());
        amqpAdmin.declareQueue(queue());
        amqpAdmin.declareBinding(binding());
    }


}