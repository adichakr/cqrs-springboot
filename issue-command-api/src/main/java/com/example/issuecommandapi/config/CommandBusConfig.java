package com.example.issuecommandapi.config;

import org.axonframework.commandhandling.distributed.AnnotationRoutingStrategy;
import org.axonframework.commandhandling.distributed.CommandBusConnector;
import org.axonframework.commandhandling.distributed.CommandRouter;
import org.axonframework.commandhandling.distributed.DistributedCommandBus;
import org.axonframework.extensions.springcloud.commandhandling.SpringCloudHttpBackupCommandRouter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CommandBusConfig {
    @Primary // to make sure this CommandBus implementation is used for autowiring
    @Bean
    public DistributedCommandBus springCloudDistributedCommandBus(
            CommandRouter commandRouter,
            CommandBusConnector commandBusConnector) {
        return DistributedCommandBus.builder()
                .commandRouter(commandRouter)
                .connector(commandBusConnector)
                .build();
    }

    @Bean
    public CommandRouter springCloudHttpBackupCommandRouter(
            DiscoveryClient discoveryClient,
            RestTemplate restTemplate,
            Registration localServiceInstance,
            @Value("${axon.distributed.spring-cloud.fallback-url}")
                    String messageRoutingInformationEndpoint) {
        return SpringCloudHttpBackupCommandRouter.builder()
                .discoveryClient(discoveryClient)
                .routingStrategy(new AnnotationRoutingStrategy())
                .restTemplate(restTemplate)
                .localServiceInstance(localServiceInstance)
                .messageRoutingInformationEndpoint(messageRoutingInformationEndpoint)
                .build();
    }

}
