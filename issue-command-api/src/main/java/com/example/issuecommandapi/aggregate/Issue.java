package com.example.issuecommandapi.aggregate;

import com.example.corelib.events.IssueAssignedEvent;
import com.example.corelib.events.IssueCreatedEvent;
import com.example.issuecommandapi.commands.AssignIssueCommand;
import com.example.issuecommandapi.commands.CreateIssueCommand;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;


@Aggregate
public class Issue {
    @AggregateIdentifier
    private String issueId;
    private String issueTitle;
    private String issueDescription;
    private Integer issueSeverity;
    private String issueStatus;
    private String assignedGroup;

    public Issue() {
    }

    @CommandHandler
    public Issue(CreateIssueCommand createIssueCommand) {
        IssueCreatedEvent issueCreatedEvent = IssueCreatedEvent.builder().build();
        BeanUtils.copyProperties(createIssueCommand, issueCreatedEvent);
        apply(issueCreatedEvent);
    }

    @CommandHandler
    public void on(AssignIssueCommand assignIssueCommand) {
        IssueAssignedEvent issueAssignedEvent = IssueAssignedEvent.builder().build();
        BeanUtils.copyProperties(assignIssueCommand, issueAssignedEvent);
        apply(issueAssignedEvent);
    }

    @EventSourcingHandler
    public void on(IssueCreatedEvent issueCreatedEvent) {
        this.issueId = issueCreatedEvent.getIssueId();
        this.issueTitle = issueCreatedEvent.getIssueTitle();
        this.issueDescription = issueCreatedEvent.getIssueDescription();
        this.issueSeverity = issueCreatedEvent.getIssueSeverity();
        this.issueStatus = issueCreatedEvent.getIssueStatus();
        this.assignedGroup = issueCreatedEvent.getAssignedGroup();
    }

    @EventSourcingHandler
    public void on(IssueAssignedEvent issueAssignedEvent) {
        this.issueStatus = issueAssignedEvent.getIssueStatus();
        this.assignedGroup = issueAssignedEvent.getAssignedGroup();
    }

}
