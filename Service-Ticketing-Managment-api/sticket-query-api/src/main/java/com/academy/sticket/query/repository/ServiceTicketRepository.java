package com.academy.sticket.query.repository;

import com.academy.sticket.query.entities.ServiceTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceTicketRepository extends JpaRepository<ServiceTicket, String> {

}
