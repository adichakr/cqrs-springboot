package com.academy.sticket.query.events.handlers;

import com.academy.complaintcore.events.ServiceTicketCreatedEvent;
import com.academy.sticket.query.entities.ServiceTicket;
import com.academy.sticket.query.repository.ServiceTicketRepository;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ServiceTicketEventHandlers {

  private final ServiceTicketRepository serviceTicketRepository;

  public ServiceTicketEventHandlers(ServiceTicketRepository serviceTicketRepository) {
    this.serviceTicketRepository = serviceTicketRepository;
  }

  @EventHandler
  public void on(ServiceTicketCreatedEvent serviceTicketCreatedEvent) {
    log.info("Inside query side EventHandler of serviceTicketCreatedEvent");
    ServiceTicket serviceTicket = new ServiceTicket();
    BeanUtils.copyProperties(serviceTicketCreatedEvent, serviceTicket);
    serviceTicketRepository.save(serviceTicket);
  }
}
