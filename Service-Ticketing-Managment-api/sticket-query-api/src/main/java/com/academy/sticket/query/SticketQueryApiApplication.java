package com.academy.sticket.query;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SticketQueryApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SticketQueryApiApplication.class, args);
	}

}
