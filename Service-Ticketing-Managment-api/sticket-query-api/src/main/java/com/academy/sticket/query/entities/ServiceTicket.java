package com.academy.sticket.query.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class ServiceTicket implements Serializable {

  @Id
  private String serviceTicketId;
  private String complaintId;
  private String userEmailAddress;
  private String userContactNumber;
  private String category;
  private String complaintTitle;
  private String complaintDescription;
  private int severity;
  private String complaintStatus;
  private String serviceTicketStatus;
  private String owner;
}
