package com.academy.sticket.commandapi.aggregate;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

import com.academy.complaintcore.commands.CreateServiceTicketCommand;
import com.academy.complaintcore.events.ServiceTicketCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

@Aggregate
@Slf4j
public class ServiceTicketAggregate {

  @AggregateIdentifier
  private String serviceTicketId;
  private String complaintId;
  private String userEmailAddress;
  private String userContactNumber;
  private String category;
  private String complaintTitle;
  private String complaintDescription;
  private int severity;
  private String complaintStatus;
  private String serviceTicketStatus;
  private String owner;

  @CommandHandler
  public ServiceTicketAggregate(CreateServiceTicketCommand createServiceTicketCommand) {
    log.info("Inside command handlers of CreateServiceTicketCommand");
    ServiceTicketCreatedEvent serviceTicketCreatedEvent = new ServiceTicketCreatedEvent();
    BeanUtils.copyProperties(createServiceTicketCommand, serviceTicketCreatedEvent);
    serviceTicketCreatedEvent.setOwner("engineer");
    serviceTicketCreatedEvent.setServiceTicketStatus("Assigned");
    apply(serviceTicketCreatedEvent);
  }

  @EventSourcingHandler
  public void on(ServiceTicketCreatedEvent serviceTicketCreatedEvent) {
    log.info("Inside EventSourcingHandler ServiceTicketCreatedEvent");
    this.serviceTicketId = serviceTicketCreatedEvent.getServiceTicketId();
    this.complaintId = serviceTicketCreatedEvent.getComplaintId();
    this.userEmailAddress = serviceTicketCreatedEvent.getUserEmailAddress();
    this.userContactNumber = serviceTicketCreatedEvent.getUserContactNumber();
    this.category = serviceTicketCreatedEvent.getCategory();
    this.complaintTitle = serviceTicketCreatedEvent.getComplaintTitle();
    this.complaintDescription = serviceTicketCreatedEvent.getComplaintDescription();
    this.severity = serviceTicketCreatedEvent.getSeverity();
    this.complaintStatus = serviceTicketCreatedEvent.getComplaintStatus();
    this.serviceTicketStatus = serviceTicketCreatedEvent.getServiceTicketStatus();
    this.owner = serviceTicketCreatedEvent.getOwner();
  }


}
