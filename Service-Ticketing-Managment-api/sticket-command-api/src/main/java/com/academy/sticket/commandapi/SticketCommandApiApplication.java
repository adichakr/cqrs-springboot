package com.academy.sticket.commandapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SticketCommandApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SticketCommandApiApplication.class, args);
	}

}
