package com.example.hello.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@RestController
@Slf4j
public class HelloController {
    @GetMapping("/hello")
    public Mono<String> sayHello() {
        log.info("hello world");
        return Mono.just("Hello world");
    }
}
