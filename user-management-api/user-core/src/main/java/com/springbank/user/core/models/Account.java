package com.springbank.user.core.models;

import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account {

  @Size(min = 2, message = "User name should be atleast two characters long")
  private String username;
  @Size(min = 7, message = "Password should be atleast serven characters long")
  private String password;
  @NotNull(message = "You should specify atleast one role")
  private List<Role> roles;
}
