package com.springbank.user.core.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "users")
public class User {

  @Id
  private String id;

  @NotEmpty(message = "first name is mandatory")
  private String firstName;
  @NotEmpty(message = "last name is mandatory")
  private String lastName;
  @Email(message = "Please provide a valid email")
  private String emailAddress;
  @NotNull(message = "Please provide user account details")
  private Account account;

}
