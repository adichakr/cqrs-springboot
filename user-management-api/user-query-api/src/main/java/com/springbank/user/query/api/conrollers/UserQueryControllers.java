package com.springbank.user.query.api.conrollers;

import com.springbank.user.query.api.dto.UserLookupResponse;
import com.springbank.user.query.api.queries.FindAllUserQuery;
import com.springbank.user.query.api.queries.FindUserByIdQuery;
import com.springbank.user.query.api.queries.SearchUsersQuery;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserQueryControllers {

  private final QueryGateway queryGateway;

  public UserQueryControllers(QueryGateway queryGateway) {
    this.queryGateway = queryGateway;
  }

  @GetMapping
  public ResponseEntity<UserLookupResponse> getAllUsers() {

    try {
      var query = new FindAllUserQuery();
      var response = queryGateway.query(query, ResponseTypes.instanceOf(UserLookupResponse.class)).join();
      if (response == null || response.getUsers() == null) {
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
      }
      return new ResponseEntity<>(response, HttpStatus.OK);
    } catch (Exception ex) {

      var safeErrorMsg = "Failed to complete getAll Users request";
      System.out.println(ex.toString());
      return new ResponseEntity<>(new UserLookupResponse(safeErrorMsg), HttpStatus.NOT_FOUND);
    }

  }

  @GetMapping("/{id}")
  public ResponseEntity<UserLookupResponse> getUserById(@PathVariable String id) {

    try {
      var query = new FindUserByIdQuery(id);
      var response = queryGateway.query(query, ResponseTypes.instanceOf(UserLookupResponse.class)).join();
      if (response == null || response.getUsers() == null) {
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
      }
      return new ResponseEntity<>(response, HttpStatus.OK);
    } catch (Exception ex) {

      var safeErrorMsg = "Failed to complete getUserById Users request";
      System.out.println(ex.toString());
      return new ResponseEntity<>(new UserLookupResponse(safeErrorMsg), HttpStatus.NOT_FOUND);
    }

  }

  @GetMapping("/filter/{filter}")
  public ResponseEntity<UserLookupResponse> searchUsersByFilter(@PathVariable String filter) {

    try {
      var query = new SearchUsersQuery(filter);
      var response = queryGateway.query(query, ResponseTypes.instanceOf(UserLookupResponse.class)).join();
      if (response == null || response.getUsers() == null) {
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
      }
      return new ResponseEntity<>(response, HttpStatus.OK);
    } catch (Exception ex) {

      var safeErrorMsg = "Failed to complete searchUsersByFilter Users request";
      System.out.println(ex.toString());
      return new ResponseEntity<>(new UserLookupResponse(safeErrorMsg), HttpStatus.NOT_FOUND);
    }

  }

}
