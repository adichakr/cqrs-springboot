package com.springbank.user.query.api.handlers;

import com.springbank.user.core.models.User;
import com.springbank.user.query.api.dto.UserLookupResponse;
import com.springbank.user.query.api.queries.FindAllUserQuery;
import com.springbank.user.query.api.queries.FindUserByIdQuery;
import com.springbank.user.query.api.queries.SearchUsersQuery;
import com.springbank.user.query.api.repositories.UserRepository;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

@Service
public class UserQueryHandlerImpl implements UserQueryHandler {

  private final UserRepository userRepository;

  public UserQueryHandlerImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  @QueryHandler
  public UserLookupResponse getUserById(FindUserByIdQuery findUserByIdQuery) {
    final Optional<User> user = userRepository.findById(findUserByIdQuery.getId());
    UserLookupResponse userLookupResponse = null;
    if (user.isPresent()) {
      userLookupResponse = new UserLookupResponse("Fetched user successfully", Arrays.asList(user.get()));
    }
    return userLookupResponse;
  }

  @Override
  @QueryHandler
  public UserLookupResponse searchUser(SearchUsersQuery searchUsersQuery) {
    final List<User> allUsers = userRepository.findUsersBySearchFilter(searchUsersQuery.getFilter());
    UserLookupResponse userLookupResponse = null;
    if (!allUsers.isEmpty()) {
      userLookupResponse = new UserLookupResponse("Fetched user successfully", allUsers);
    }
    return userLookupResponse;

  }

  @Override
  @QueryHandler
  public UserLookupResponse getAllUsers(FindAllUserQuery findAllUserQuery) {
    final List<User> allUsers = userRepository.findAll();
    UserLookupResponse userLookupResponse = null;
    if (!allUsers.isEmpty()) {
      userLookupResponse = new UserLookupResponse("Fetched all user successfully", allUsers);
    }
    return userLookupResponse;
  }
}
