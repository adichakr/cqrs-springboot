package com.springbank.user.query.api.repositories;

import com.springbank.user.core.models.User;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

  @Query("{'$or':[{'firstName' : { $regex: ?0 }},{'lastName' : { $regex: ?0 }},{'emailAddress' : { $regex: ?0 }},"
      + "{'account.username' : { $regex: ?0 }} ]}")
  public List<User> findUsersBySearchFilter(String filter);

}
