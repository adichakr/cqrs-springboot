package com.springbank.user.cmd.api.aggregates;

import com.springbank.user.cmd.api.commands.RegisterUserCommand;
import com.springbank.user.cmd.api.commands.RemoveUserCommand;
import com.springbank.user.cmd.api.commands.UpdateUserCommand;
import com.springbank.user.cmd.api.security.PasswordEncoder;
import com.springbank.user.cmd.api.security.PasswordEncoderImpl;
import com.springbank.user.core.events.UserRegisteredEvent;
import com.springbank.user.core.events.UserRemovedEvent;
import com.springbank.user.core.events.UserUpdatedEvent;
import com.springbank.user.core.models.User;
import java.util.UUID;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
public class UserAggregate {

  @AggregateIdentifier
  private String id;
  private User user;

  private final PasswordEncoder passwordEncoder;

  public UserAggregate() {
    passwordEncoder = new PasswordEncoderImpl();
  }

  @CommandHandler
  public UserAggregate(RegisterUserCommand registerUserCommand) {
    var newUser = registerUserCommand.getUser();
    newUser.setId(registerUserCommand.getId());
    var password = newUser.getAccount().getPassword();
    passwordEncoder = new PasswordEncoderImpl();
    var hashedPassword = passwordEncoder.hashedPassword(password);
    newUser.getAccount().setPassword(hashedPassword);
    var userRegisteredEvent = UserRegisteredEvent.builder().id(registerUserCommand.getId()).user(newUser).build();
    AggregateLifecycle.apply(userRegisteredEvent);

  }

  @CommandHandler
  public void handle(UpdateUserCommand updateUserCommand) {
    var updatedUser = updateUserCommand.getUser();
    updatedUser.setId(updateUserCommand.getId());
    var password = updatedUser.getAccount().getPassword();
    var hashedPassword = passwordEncoder.hashedPassword(password);
    updatedUser.getAccount().setPassword(hashedPassword);
    var userUpdatedEvent = UserUpdatedEvent.builder().id(UUID.randomUUID().toString()).user(updatedUser).build();
    AggregateLifecycle.apply(userUpdatedEvent);

  }

  @CommandHandler
  public void handle(RemoveUserCommand removeUserCommand) {
    var event = new UserRemovedEvent();
    event.setId(removeUserCommand.getId());
    AggregateLifecycle.apply(event);
  }


  @EventSourcingHandler
  public void on(UserRegisteredEvent userRegisteredEvent) {
    this.id = userRegisteredEvent.getId();
    this.user = userRegisteredEvent.getUser();
  }

  @EventSourcingHandler
  public void on(UserUpdatedEvent userUpdatedEvent) {
    this.user = userUpdatedEvent.getUser();
  }

  @EventSourcingHandler
  public void on(UserRemovedEvent userRemovedEvent) {
    AggregateLifecycle.markDeleted();
  }

}
