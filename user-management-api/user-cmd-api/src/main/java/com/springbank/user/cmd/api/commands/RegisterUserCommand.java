package com.springbank.user.cmd.api.commands;

import com.springbank.user.core.models.User;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@Builder
public class RegisterUserCommand {

  @TargetAggregateIdentifier
  private String id;
  @Valid
  @NotNull(message = "user information is mandatory")
  private User user;

}
