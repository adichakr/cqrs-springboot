package com.springbank.user.cmd.api.controllers;

import com.springbank.user.cmd.api.commands.RegisterUserCommand;
import com.springbank.user.cmd.api.commands.RemoveUserCommand;
import com.springbank.user.cmd.api.commands.UpdateUserCommand;
import com.springbank.user.cmd.api.dto.RegisterUserResponse;
import java.util.UUID;
import javax.validation.Valid;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

  private final CommandGateway commandGateway;

  public UserController(CommandGateway commandGateway) {
    this.commandGateway = commandGateway;
  }

  @PostMapping
  public ResponseEntity<RegisterUserResponse> registerUser(@Valid @RequestBody RegisterUserCommand registerUserCommand) {
    registerUserCommand.setId(UUID.randomUUID().toString());
    try {
      commandGateway.send(registerUserCommand);
      return new ResponseEntity<>(new RegisterUserResponse(registerUserCommand.getId(), "User Successfully Registered"),
          HttpStatus.CREATED);
    } catch (Exception ex) {
      var safeErrorMessage = "Error while processing user request for id  - " + registerUserCommand.getId();
      System.out.println(ex.toString());
      return new ResponseEntity<>(new RegisterUserResponse(registerUserCommand.getId(), safeErrorMessage),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/{id}")
  public ResponseEntity<RegisterUserResponse> updateUser(@Valid @RequestBody UpdateUserCommand updateUserCommand,
      @PathVariable String id) {

    try {
      updateUserCommand.setId(id);
      commandGateway.send(updateUserCommand);
      return new ResponseEntity<>(new RegisterUserResponse(updateUserCommand.getId(), "User Successfully updated"),
          HttpStatus.OK);
    } catch (Exception ex) {
      var safeErrorMessage = "Error while processing user request for id  - " + updateUserCommand.getId();
      System.out.println(ex.toString());
      return new ResponseEntity<>(new RegisterUserResponse(updateUserCommand.getId(), safeErrorMessage),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }


  }

  @DeleteMapping("/{id}")
  public ResponseEntity<RegisterUserResponse> removeUser(@PathVariable String id) {

    try {
      commandGateway.send(new RemoveUserCommand(id));
      return new ResponseEntity<>(new RegisterUserResponse(id, "User Successfully deleted"),
          HttpStatus.NO_CONTENT);
    } catch (Exception ex) {
      var safeErrorMessage = "Error while processing user request for id  - " + id;
      System.out.println(ex.toString());
      return new ResponseEntity<>(new RegisterUserResponse(id, safeErrorMessage),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }


  }


}
