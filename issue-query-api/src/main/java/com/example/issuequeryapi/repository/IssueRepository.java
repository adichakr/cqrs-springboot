package com.example.issuequeryapi.repository;

import com.example.issuequeryapi.entity.IssueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IssueRepository extends JpaRepository<IssueEntity, String> {
}
