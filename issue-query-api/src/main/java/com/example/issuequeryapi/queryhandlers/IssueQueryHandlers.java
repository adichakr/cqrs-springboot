package com.example.issuequeryapi.queryhandlers;

import com.example.corelib.events.IssueAssignedEvent;
import com.example.corelib.events.IssueCreatedEvent;
import com.example.issuequeryapi.entity.IssueEntity;
import com.example.issuequeryapi.service.IssueQueryHandlerService;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@ProcessingGroup("issueConsumer")
@Slf4j
public class IssueQueryHandlers {
    private final IssueQueryHandlerService issueQueryHandlerService;

    public IssueQueryHandlers(IssueQueryHandlerService issueQueryHandlerService) {
        this.issueQueryHandlerService = issueQueryHandlerService;
    }

    @EventHandler
    public void on(IssueCreatedEvent event) {
        log.info("Inside Event Handler of IssueCreatedEvent ");
        issueQueryHandlerService.saveIssue(event);
    }

    @EventHandler
    public void on(IssueAssignedEvent event) {
        log.info("Inside Event Handler of IssueAssignedEvent ");
        IssueEntity issue = issueQueryHandlerService.findIssue(event.getIssueId()).get();
        issue.setIssueStatus(event.getIssueStatus());
        issue.setAssignedGroup(event.getAssignedGroup());
        issueQueryHandlerService.saveIssue(issue);
    }
}
