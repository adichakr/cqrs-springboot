package com.example.issuequeryapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class IssueQueryApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(IssueQueryApiApplication.class, args);
    }

}
