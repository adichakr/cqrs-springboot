package com.example.issuequeryapi.service;

import com.example.corelib.events.IssueCreatedEvent;
import com.example.issuequeryapi.entity.IssueEntity;

import java.util.Optional;

public interface IssueQueryHandlerService {
    void saveIssue(IssueCreatedEvent issueCreatedEvent);

    void saveIssue(IssueEntity issueEntity);

    Optional<IssueEntity> findIssue(String issueId);
}
