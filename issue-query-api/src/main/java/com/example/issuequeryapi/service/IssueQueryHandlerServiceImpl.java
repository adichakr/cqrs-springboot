package com.example.issuequeryapi.service;

import com.example.corelib.events.IssueCreatedEvent;
import com.example.issuequeryapi.entity.IssueEntity;
import com.example.issuequeryapi.repository.IssueRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class IssueQueryHandlerServiceImpl implements IssueQueryHandlerService {
    public final IssueRepository issueRepository;

    public IssueQueryHandlerServiceImpl(IssueRepository issueRepository) {
        this.issueRepository = issueRepository;
    }

    @Override
    public void saveIssue(IssueCreatedEvent issueCreatedEvent) {
        log.info("saveIssue ------> {}", issueCreatedEvent);
        IssueEntity issueEntity = new IssueEntity();
        BeanUtils.copyProperties(issueCreatedEvent, issueEntity);
        issueRepository.save(issueEntity);
    }

    @Override
    public void saveIssue(IssueEntity issueEntity) {
        issueRepository.save(issueEntity);
    }

    @Override
    public Optional<IssueEntity> findIssue(String issueId) {
        return issueRepository.findById(issueId);
    }
}
