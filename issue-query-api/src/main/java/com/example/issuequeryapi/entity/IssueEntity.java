package com.example.issuequeryapi.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "Issues")
@Data
@Getter
@Setter
public class IssueEntity implements Serializable {
    @Id
    @Column(name = "issueId", nullable = false)
    private String issueId;
    private String issueTitle;
    private String issueDescription;
    private Integer issueSeverity;
    private String issueStatus;
    private String assignedGroup;

}
