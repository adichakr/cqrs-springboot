package com.example.jiraticketapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JiraTicketApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiraTicketApiApplication.class, args);
    }

}
