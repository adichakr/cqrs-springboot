package com.example.jiraticketapi.aggregate;

import com.example.corelib.commands.CreateJiraTicketCommand;
import com.example.corelib.events.JiraTicketCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
@Slf4j
public class JiraTicket {
    @AggregateIdentifier
    private String issueId;
    private String jiraNumber;
    private String jiraStatus;
    private String ticketOwner;

    @CommandHandler
    public JiraTicket(CreateJiraTicketCommand createJiraTicketCommand) {
        log.info("Inside Jira Ticket Aggregate createJiraTicketCommand - {}", createJiraTicketCommand);
        apply(JiraTicketCreatedEvent.builder().issueId(createJiraTicketCommand.getIssueId())
                .jiraNumber(createJiraTicketCommand.getJiraNumber())
                .jiraStatus(createJiraTicketCommand.getJiraStatus())
                .ticketOwner(createJiraTicketCommand.getTicketOwner()).build());
    }

    @EventSourcingHandler
    public void on(JiraTicketCreatedEvent jiraTicketCreatedEvent) {
        log.info("Inside Jira Ticket EventSourcingHandler jiraTicketCreatedEvent- {}", jiraTicketCreatedEvent);
        this.issueId = jiraTicketCreatedEvent.getIssueId();
        this.jiraNumber = jiraTicketCreatedEvent.getJiraNumber();
        this.jiraStatus = jiraTicketCreatedEvent.getJiraStatus();
        this.ticketOwner = jiraTicketCreatedEvent.getTicketOwner();
    }
}
