package com.example.complaint.query.api.repositories;

import com.example.complaint.query.api.entities.Complaint;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComplaintRepository extends JpaRepository<Complaint, String> {

  List<Complaint> findByComplaintStatus(String complaintStatus);
}
