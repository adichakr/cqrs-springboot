package com.example.complaint.query.api.services;

import com.example.complaint.query.api.entities.Complaint;
import java.util.List;

public interface ComplaintQueryService {

  List<Complaint> getAllComplaints();

  List<Complaint> getComplaintsByStatus(String complaintStatus);
}
