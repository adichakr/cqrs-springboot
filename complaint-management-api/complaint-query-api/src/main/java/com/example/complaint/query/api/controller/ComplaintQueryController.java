package com.example.complaint.query.api.controller;

import com.example.complaint.query.api.entities.Complaint;
import com.example.complaint.query.api.services.ComplaintQueryService;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/complaints")
public class ComplaintQueryController {

  private final ComplaintQueryService complaintQueryService;

  public ComplaintQueryController(ComplaintQueryService complaintQueryService) {
    this.complaintQueryService = complaintQueryService;
  }

  @GetMapping
  public ResponseEntity<List<Complaint>> getAllComplaints() {
    return ResponseEntity.status(HttpStatus.OK).body(complaintQueryService.getAllComplaints());
  }

  @GetMapping("/{status}")
  public ResponseEntity<List<Complaint>> getComplaintsByStatus(@PathVariable String status) {
    return ResponseEntity.status(HttpStatus.OK).body(complaintQueryService.getComplaintsByStatus(status));

  }
}
