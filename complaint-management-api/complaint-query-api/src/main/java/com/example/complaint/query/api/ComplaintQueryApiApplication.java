package com.example.complaint.query.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComplaintQueryApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComplaintQueryApiApplication.class, args);
	}

}
