package com.example.complaint.query.api.queries.handlers;

import com.academy.complaintcore.events.ComplaintRegisteredEvent;
import com.example.complaint.query.api.entities.Complaint;
import com.example.complaint.query.api.repositories.ComplaintRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class ComplaintEventHandler {

  private final ComplaintRepository complaintRepository;

  public ComplaintEventHandler(ComplaintRepository complaintRepository) {
    this.complaintRepository = complaintRepository;
  }

  @EventHandler
  public void on(ComplaintRegisteredEvent complaintRegisteredEvent) {
    Complaint complaint = new Complaint();
    BeanUtils.copyProperties(complaintRegisteredEvent, complaint);
    complaintRepository.save(complaint);

  }


}
