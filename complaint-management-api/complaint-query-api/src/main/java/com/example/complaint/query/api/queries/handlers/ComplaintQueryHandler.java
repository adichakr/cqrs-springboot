package com.example.complaint.query.api.queries.handlers;

import com.example.complaint.query.api.entities.Complaint;
import com.example.complaint.query.api.queries.FindAllComplaintsQuery;
import com.example.complaint.query.api.queries.FindComplaintsByStatusQuery;
import com.example.complaint.query.api.repositories.ComplaintRepository;
import java.util.List;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

@Component
public class ComplaintQueryHandler {

  private final ComplaintRepository complaintRepository;

  public ComplaintQueryHandler(ComplaintRepository complaintRepository) {
    this.complaintRepository = complaintRepository;
  }

  @QueryHandler
  public List<Complaint> on(FindAllComplaintsQuery findAllComplaintsQuery) {
    return complaintRepository.findAll();
  }

  @QueryHandler
  public List<Complaint> on(FindComplaintsByStatusQuery findComplaintsByStatusQuery) {
    String complaintStatus = findComplaintsByStatusQuery.getComplaintStatus();
    return complaintRepository.findByComplaintStatus(complaintStatus);
  }

}
