package com.example.complaint.query.api.services.impl;

import com.example.complaint.query.api.entities.Complaint;
import com.example.complaint.query.api.queries.FindAllComplaintsQuery;
import com.example.complaint.query.api.queries.FindComplaintsByStatusQuery;
import com.example.complaint.query.api.services.ComplaintQueryService;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.stereotype.Service;

@Service
public class ComplaintQueryServiceImpl implements ComplaintQueryService {

  private final QueryGateway queryGateway;

  public ComplaintQueryServiceImpl(QueryGateway queryGateway) {
    this.queryGateway = queryGateway;
  }

  @Override
  public List<Complaint> getAllComplaints() {
    FindAllComplaintsQuery findAllComplaintsQuery = new FindAllComplaintsQuery();
    final CompletableFuture<List<Complaint>> allComplaints = queryGateway
        .query(findAllComplaintsQuery, ResponseTypes.multipleInstancesOf(Complaint.class));
    return allComplaints.join();
  }

  @Override
  public List<Complaint> getComplaintsByStatus(String complaintStatus) {
    FindComplaintsByStatusQuery findAllComplaintsQueryByStatus = new FindComplaintsByStatusQuery(complaintStatus);
    final CompletableFuture<List<Complaint>> allComplaintsbyStatus = queryGateway
        .query(findAllComplaintsQueryByStatus, ResponseTypes.multipleInstancesOf(Complaint.class));
    return allComplaintsbyStatus.join();

  }
}
