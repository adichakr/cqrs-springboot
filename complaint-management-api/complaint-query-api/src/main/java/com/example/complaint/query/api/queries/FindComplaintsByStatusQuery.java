package com.example.complaint.query.api.queries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class FindComplaintsByStatusQuery {

  String complaintStatus;

}
