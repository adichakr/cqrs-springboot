package com.example.dummymicroservicesmonitoringstack.controllers;

import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class DummyController {
    @GetMapping("/")
    public String greeting() {
        log.info("Inside the greeting controller");
        return "Hello World";
    }

    @GetMapping("/{name}")
    @Timed(value = "greeting.time", description = "Time taken to return greeting")
    public String greetingWithName(@PathVariable String name) {
        log.info("Inside the greetingWithName controller");
        log.info("Inside the greetingWithName controller and the pathvariable is {}", name);
        return "Good Morning - " + name;
    }


}
