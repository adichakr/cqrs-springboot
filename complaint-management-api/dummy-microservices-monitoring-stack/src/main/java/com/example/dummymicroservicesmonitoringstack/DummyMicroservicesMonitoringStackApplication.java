package com.example.dummymicroservicesmonitoringstack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DummyMicroservicesMonitoringStackApplication {

    public static void main(String[] args) {
        SpringApplication.run(DummyMicroservicesMonitoringStackApplication.class, args);
    }

}
