package com.academy.complaintcore.commands;

import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@Builder
public class RegisterComplaintCommand {

  @TargetAggregateIdentifier
  private String complaintId;
  private String userEmailAddress;
  private String userContactNumber;
  private String category;
  private String complaintTitle;
  private String complaintDescription;
  private int severity;
  private String complaintStatus;
}
