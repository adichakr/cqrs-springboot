package com.academy.complaintcore.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceTicketCreatedEvent {

  private String serviceTicketId;
  private String complaintId;
  private String userEmailAddress;
  private String userContactNumber;
  private String category;
  private String complaintTitle;
  private String complaintDescription;
  private int severity;
  private String complaintStatus;
  private String serviceTicketStatus;
  private String owner;

}
