package com.academy.complaintcore.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ComplaintRegisteredEvent {

  private String complaintId;
  private String userEmailAddress;
  private String userContactNumber;
  private String category;
  private String complaintTitle;
  private String complaintDescription;
  private int severity;
  private String complaintStatus;
}
