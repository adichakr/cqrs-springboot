package com.academy.compaintcommandapi.exception.handlers;

import com.academy.compaintcommandapi.exception.error.ComplaintApplicationError;
import java.time.LocalDate;
import org.axonframework.commandhandling.CommandExecutionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class ComplaintExceptionHandler {

  private final ComplaintApplicationError complaintApplicationError;

  public ComplaintExceptionHandler(
      ComplaintApplicationError complaintApplicationError) {
    this.complaintApplicationError = complaintApplicationError;
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Object> handleRequestValidationException(MethodArgumentNotValidException ex, WebRequest request) {
    final ComplaintApplicationError error = new ComplaintApplicationError();
    complaintApplicationError.setErrorCode("1000");
    complaintApplicationError.setErrorMessage("Invalid Request Body - " + ex.getLocalizedMessage());
    complaintApplicationError.setPath(request.getContextPath());
    complaintApplicationError.setTimestamp(LocalDate.now());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
  }

  @ExceptionHandler(CommandExecutionException.class)
  public ResponseEntity<Object> handleCommandExecutionException(CommandExecutionException ex, WebRequest request) {
    final ComplaintApplicationError error = new ComplaintApplicationError();
    complaintApplicationError.setErrorCode("1001");
    complaintApplicationError.setErrorMessage("Error while executing commands - " + ex.getLocalizedMessage());
    complaintApplicationError.setPath(request.getContextPath());
    complaintApplicationError.setTimestamp(LocalDate.now());
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
  }


}
