package com.academy.compaintcommandapi.constants;

public enum ComplaintStatus {
  RAISED, WORK_IN_PROGRESS, RESOLVED, REJECTED

}
