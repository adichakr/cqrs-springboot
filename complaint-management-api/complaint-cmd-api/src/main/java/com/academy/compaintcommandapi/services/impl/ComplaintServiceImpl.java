package com.academy.compaintcommandapi.services.impl;

import com.academy.complaintcore.commands.RegisterComplaintCommand;
import com.academy.compaintcommandapi.constants.ComplaintStatus;
import com.academy.compaintcommandapi.dtos.ComplaintDTO;
import com.academy.compaintcommandapi.services.ComplaintService;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ComplaintServiceImpl implements ComplaintService {

  private final CommandGateway commandGateway;

  public ComplaintServiceImpl(CommandGateway commandGateway) {
    this.commandGateway = commandGateway;
  }

  @Override
  public String registerComplaint(ComplaintDTO complaintDTO) {
    log.info("Inside service - registerComplaint");
    RegisterComplaintCommand registerComplaintCommand = RegisterComplaintCommand.builder().build();
    BeanUtils.copyProperties(complaintDTO, registerComplaintCommand);
    //set the client Unique identifier
    registerComplaintCommand.setComplaintId(UUID.randomUUID().toString());
    //set the complaint status to raised
    registerComplaintCommand.setComplaintStatus(ComplaintStatus.RAISED.name());
    log.info("Raising Register Complaint command");
    return commandGateway.sendAndWait(registerComplaintCommand);

  }
}
