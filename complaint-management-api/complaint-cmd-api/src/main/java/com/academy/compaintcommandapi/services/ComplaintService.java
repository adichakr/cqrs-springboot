package com.academy.compaintcommandapi.services;

import com.academy.compaintcommandapi.dtos.ComplaintDTO;

public interface ComplaintService {

  String registerComplaint(ComplaintDTO complaintDTO);
}
