package com.academy.compaintcommandapi.sagas;

import com.academy.complaintcore.commands.CreateServiceTicketCommand;
import com.academy.complaintcore.events.ComplaintRegisteredEvent;
import com.academy.complaintcore.events.ServiceTicketCreatedEvent;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

@Saga
@Slf4j
public class ComplaintSagas {

  public ComplaintSagas() {

  }

  @Autowired
  private transient CommandGateway commandGateway;


  @StartSaga
  @SagaEventHandler(associationProperty = "complaintId")
  public void on(ComplaintRegisteredEvent complaintRegisteredEvent) {
    CreateServiceTicketCommand createServiceTicketCommand =
        CreateServiceTicketCommand.builder().
            serviceTicketId(UUID.randomUUID().toString())
            .complaintId(complaintRegisteredEvent.getComplaintId()).
            category(complaintRegisteredEvent.getCategory())
            .userEmailAddress(complaintRegisteredEvent.getUserEmailAddress())
            .userContactNumber(complaintRegisteredEvent.getUserContactNumber())
            .complaintTitle(complaintRegisteredEvent.getComplaintTitle())
            .complaintDescription(complaintRegisteredEvent.getComplaintDescription())
            .complaintStatus(complaintRegisteredEvent.getComplaintStatus())
            .category(complaintRegisteredEvent.getCategory()).
            build();
    commandGateway.sendAndWait(createServiceTicketCommand);
  }

  @EndSaga
  @SagaEventHandler(associationProperty = "complaintId")
  public void on(ServiceTicketCreatedEvent serviceTicketCreatedEvent) {
    log.info("Inside saga of serviceTicketCreatedEvent ");
  }

}
