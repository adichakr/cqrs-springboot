package com.academy.compaintcommandapi.controllers;

import com.academy.compaintcommandapi.dtos.ComplaintDTO;
import com.academy.compaintcommandapi.services.ComplaintService;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/complaints")
@Slf4j
public class ComplaintControllers {

  private final ComplaintService complaintService;

  public ComplaintControllers(ComplaintService complaintService) {
    this.complaintService = complaintService;
  }

  @PostMapping
  public ResponseEntity<String> raiseComplaint(@Valid @RequestBody ComplaintDTO complaintDTO) {
    log.info("Inside raiseComplaint  ");
    return ResponseEntity.status(HttpStatus.CREATED).body(complaintService.registerComplaint(complaintDTO));
  }


}
