package com.academy.compaintcommandapi.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@AllArgsConstructor
public class ComplaintDTO {

  @Email
  private String userEmailAddress;
  @NotEmpty
  private String userContactNumber;
  @NotEmpty
  private String category;
  @NotEmpty
  private String complaintTitle;
  @NotEmpty
  private String complaintDescription;
  @Positive
  private int severity;

}
