package com.academy.compaintcommandapi.aggregate;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

import com.academy.complaintcore.commands.RegisterComplaintCommand;
import com.academy.complaintcore.events.ComplaintRegisteredEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

@Aggregate
public class ComplaintAggregate {

  @AggregateIdentifier
  private String complaintId;

  private String userEmailAddress;
  private String userContactNumber;
  private String category;
  private String complaintTitle;
  private String complaintDescription;
  private int severity;
  private String complaintStatus;

  @CommandHandler
  public ComplaintAggregate(RegisterComplaintCommand registerComplaintCommand) {
    final ComplaintRegisteredEvent complaintRegisteredEvent = ComplaintRegisteredEvent.builder().build();
    BeanUtils.copyProperties(registerComplaintCommand, complaintRegisteredEvent);
    apply(complaintRegisteredEvent);
  }


  @EventSourcingHandler
  public void on(ComplaintRegisteredEvent complaintRegisteredEvent) {
    this.complaintId = complaintRegisteredEvent.getComplaintId();
    this.userEmailAddress = complaintRegisteredEvent.getUserEmailAddress();
    this.userContactNumber = complaintRegisteredEvent.getUserContactNumber();
    this.category = complaintRegisteredEvent.getCategory();
    this.complaintTitle = complaintRegisteredEvent.getComplaintTitle();
    this.complaintDescription = complaintRegisteredEvent.getComplaintDescription();
    this.severity = complaintRegisteredEvent.getSeverity();
    this.complaintStatus = complaintRegisteredEvent.getComplaintStatus();

  }

}
