package com.academy.compaintcommandapi.exception.error;

import java.time.LocalDate;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class ComplaintApplicationError {

  private String errorMessage;
  private String errorCode;
  private String path;
  private LocalDate timestamp;

}
