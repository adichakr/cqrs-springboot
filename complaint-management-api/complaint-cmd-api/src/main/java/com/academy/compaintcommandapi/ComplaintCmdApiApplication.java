package com.academy.compaintcommandapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ComplaintCmdApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComplaintCmdApiApplication.class, args);
    }

}
