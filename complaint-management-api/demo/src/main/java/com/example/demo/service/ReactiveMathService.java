package com.example.demo.service;

import com.example.demo.dto.Response;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Service
public class ReactiveMathService {

    public Mono<Response> findSquare(int input) {
        return Mono.fromSupplier(() -> input * input).map(v -> new Response(v));
    }

    public Flux<Response> findMultiplicationTable(int input) {
        return Flux.range(1, 10)/*.doOnNext(i -> {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        })*/
                .delayElements(Duration.of(2, ChronoUnit.SECONDS))
                .doOnNext(i -> System.out.printf("Reactive math service processing"))
                .map(i -> new Response(i * input));
    }

}
