package com.example.demo.controller;

import com.example.demo.dto.Response;
import com.example.demo.service.ReactiveMathService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/math")
public class ReactiveMathController {

    private final ReactiveMathService reactiveMathService;

    public ReactiveMathController(ReactiveMathService reactiveMathService) {
        this.reactiveMathService = reactiveMathService;
    }

    @GetMapping("/square/{number}")
    public Mono<Response> findSquare(@PathVariable int number) {
        return reactiveMathService.findSquare(number);
    }


    @GetMapping(value = "/table/{number}",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Response> findTables(@PathVariable int number) {
        return reactiveMathService.findMultiplicationTable(number);
    }

}
